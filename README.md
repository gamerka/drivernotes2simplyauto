# DriverNotes2SimplyAuto
������������ ���� ����������� �� [DriverNotes](http://www.drivernotes.net/) � ������ [SimplyAuto](https://simplyauto.app/).
## �������������
### �������� �������
������ ������ � VS 2017 �� .Net 4.7.2
### ��������� � ����� ���� �� DriverNotes
�������� ������ �� �� ������ ```http://www.drivernotes.net/profile_car_import.php?act=export&user_id={id ������}&car_id={id ������}```
���� � ����� ������ ����� ����� ���� �����_������.csv (��� ���������� �� ��������� ��� ��� � ����� ����������).
����� � ������ ������ ����� ����� ��������� � SimplyAuto.

### �������������� ����� �� ��������� ��������:
- ���������� ����� � ������ �� DriverNotes2SimplyAuto.exe
- � ��������� ������ ��������� ```DriverNotes2SimplyAuto.exe {���� � ����� � ������}```
- ���������� ���� � ����� � DriverNotes2SimplyAuto.exe � ��������� exe

����� ����������� ����� � ������ DriverNotes �������� ��� �����: Fuel_Log.csv � Vehicles.csv

### �������� �������������� �� ��������� ����� � SimplyAuto
����� ����� ��� ������� ������ ���� ��� ����� � Google Disk, ����� ������������ ������ ������ ��� ���������� ����� � ��������� ��������������.
